package library.management.Entity;

public class Librarian {
    public String librarianId;
    public String libName;
    public String password;

    public Librarian() {
    }

    public Librarian(String librarianId, String libName, String password) {
        this.librarianId = librarianId;
        this.libName = libName;
        this.password = password;
    }

}
