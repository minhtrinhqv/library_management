package library.management.Entity;

public class Member {
    public int memberId;
    public String memberName;
    public String yearOfBirth;

    public Member() {
    }

    public String getMemberName() {
        return memberName;
    }

    public void setMemberName(String memberName) {
        this.memberName = memberName;
    }

}
