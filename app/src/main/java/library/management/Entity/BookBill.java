package library.management.Entity;

import java.sql.Date;

public class BookBill {
    public int billId;
    public String librarianId;
    public int memberId;
    public int bookId;
    public int price;
    public int returned;
    public Date date;

    public BookBill() {
    }

    public int getBillId() {
        return billId;
    }

    public void setBillId(int billId) {
        this.billId = billId;
    }

    public int getBookId() {
        return bookId;
    }

    public void setBookId(int bookId) {
        this.bookId = bookId;
    }


}
