package library.management.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import library.management.Entity.BookType;
import library.management.R;

public class BookTypeSpinnerAdapter extends ArrayAdapter<BookType> {
    private final Context context;
    private final ArrayList<BookType> lists;
    TextView tvBookTypeIdSp, tvBookTypeSp;

    public BookTypeSpinnerAdapter(@NonNull Context context, ArrayList<BookType> lists) {
        super(context, 0, lists);
        this.context = context;
        this.lists = lists;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.booktype_item_spinner, null);
        }
        final BookType item = lists.get(position);
        if (item != null) {
            tvBookTypeIdSp = v.findViewById(R.id.tvBookTypeIdSp);
            tvBookTypeIdSp.setText(item.bookTypeId + ".");
            tvBookTypeSp = v.findViewById(R.id.tvBookTypeSp);
            tvBookTypeSp.setText(item.bookType);
        }
        return v;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.booktype_item_spinner, null);
        }
        final BookType item = lists.get(position);
        if (item != null) {
            tvBookTypeIdSp = v.findViewById(R.id.tvBookTypeIdSp);
            tvBookTypeIdSp.setText(item.bookTypeId + ".");
            tvBookTypeSp = v.findViewById(R.id.tvBookTypeSp);
            tvBookTypeSp.setText(item.bookType);
        }
        return v;
    }
}
