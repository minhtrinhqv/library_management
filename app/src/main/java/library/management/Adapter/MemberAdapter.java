package library.management.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import library.management.Entity.Member;
import library.management.Fragments.MemberFragment;
import library.management.R;

public class MemberAdapter extends ArrayAdapter<Member> {
    private final Context context;
    private final ArrayList<Member> lists;
    MemberFragment fragment;
    TextView tvMemberId, tvMemberName, tvYearOfBirth;
    ImageView imgDelM;

    public MemberAdapter(@NonNull Context context, MemberFragment fragment, ArrayList<Member> lists) {
        super(context, 0, lists);
        this.context = context;
        this.lists = lists;
        this.fragment = fragment;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.member_item, null);
        }
        final Member item = lists.get(position);
        if (item != null) {

            tvMemberId = v.findViewById(R.id.tvMemberId);
            tvMemberId.setText(getContext().getResources().getString(R.string.ed_member_id) + item.memberId);
            tvMemberName = v.findViewById(R.id.tvMemberName);
            tvMemberName.setText(getContext().getResources().getString(R.string.ed_member_name) + item.memberName);
            tvYearOfBirth = v.findViewById(R.id.tvYearOfBirth);
            tvYearOfBirth.setText(getContext().getResources().getString(R.string.ed_birthday) + item.yearOfBirth);

            imgDelM = v.findViewById(R.id.imgDelM);
        }
        imgDelM.setOnClickListener(v1 -> {
            assert item != null;
            fragment.xoa(String.valueOf(item.memberId));
        });
        return v;
    }
}
