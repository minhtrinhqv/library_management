package library.management.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import library.management.Entity.Book;
import library.management.Entity.BookType;
import library.management.Fragments.BookFragment;
import library.management.MyDatabase.BookTypeDAO;
import library.management.R;

public class BookAdapter extends ArrayAdapter<Book> {
    private final Context context;
    private final ArrayList<Book> lists;
    BookFragment fragment;
    TextView tvBookId, tvBookName, tvPrice, tvBookTypeId;
    ImageView imgDel;

    public BookAdapter(@NonNull Context context, BookFragment fragment, ArrayList<Book> lists) {
        super(context, 0, lists);
        this.context = context;
        this.lists = lists;
        this.fragment = fragment;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.book_item, null);
        }
        final Book item = lists.get(position);
        if (item != null) {
            BookTypeDAO bookTypeDAO = new BookTypeDAO(context);
            BookType bookType = bookTypeDAO.getID(String.valueOf(item.bookTypeId));
            String bookTypeNAme = bookType.bookType;
            tvBookId = v.findViewById(R.id.tvBookId);
            tvBookId.setText(getContext().getResources().getString(R.string.ed_book_id) + item.bookId);
            tvBookName = v.findViewById(R.id.tvBookName);
            tvBookName.setText(getContext().getResources().getString(R.string.ed_book_name)+ item.bookName);
            tvPrice = v.findViewById(R.id.tvPrice);
            tvPrice.setText(getContext().getResources().getString(R.string.ed_price)+ item.price);
            tvBookTypeId = v.findViewById(R.id.tvBookTypeId);
            tvBookTypeId.setText(getContext().getResources().getString(R.string.ed_book_type)+ bookTypeNAme);

            imgDel = v.findViewById(R.id.imgDelB);
        }
        imgDel.setOnClickListener(v1 -> {
            assert item != null;
            fragment.xoa(String.valueOf(item.bookId));
        });
        return v;
    }
}

