package library.management.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import library.management.Entity.Book;
import library.management.R;

public class BookSpinnerAdapter extends ArrayAdapter<Book> {
    private final Context context;
    private final ArrayList<Book> lists;
    TextView tvBookTypeSp, tvBookNameSp;

    public BookSpinnerAdapter(@NonNull Context context, ArrayList<Book> lists) {
        super(context, 0, lists);
        this.context = context;
        this.lists = lists;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.book_item_spinner, null);
        }
        final Book item = lists.get(position);
        if (item != null) {
            tvBookTypeSp = v.findViewById(R.id.tvBookTypeSp);
            tvBookTypeSp.setText(item.bookTypeId + ".");
            tvBookNameSp = v.findViewById(R.id.tvBookNameSp);
            tvBookNameSp.setText(item.bookName);
        }
        return v;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.book_item_spinner, null);
        }
        final Book item = lists.get(position);
        if (item != null) {
            tvBookTypeSp = v.findViewById(R.id.tvBookTypeSp);
            tvBookTypeSp.setText(item.bookTypeId + ".");
            tvBookNameSp = v.findViewById(R.id.tvBookNameSp);
            tvBookNameSp.setText(item.bookName);
        }
        return v;
    }
}
