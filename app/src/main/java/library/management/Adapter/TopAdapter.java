package library.management.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import library.management.Entity.Top;
import library.management.Fragments.TopFragment;
import library.management.R;

public class TopAdapter extends ArrayAdapter<Top> {
    private final Context context;
    private final ArrayList<Top> lists;
    TopFragment topFragment;
    TextView tvTopBook, tvTopCount;

    public TopAdapter(@NonNull Context context, TopFragment topFragment, ArrayList<Top> lists) {
        super(context, 0, lists);
        this.context = context;
        this.lists = lists;
        this.topFragment = topFragment;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.top_item, null);
        }
        final Top item = lists.get(position);
        if (item != null) {
            tvTopBook = v.findViewById(R.id.tvTopBook);
            tvTopBook.setText(getContext().getResources().getString(R.string.ed_book_name) + item.bookName);
            tvTopCount = v.findViewById(R.id.tvTopCount);
            tvTopCount.setText(getContext().getResources().getString(R.string.ed_count) + item.count);

        }
        return v;
    }
}
