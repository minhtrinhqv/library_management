package library.management.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import library.management.Entity.BookType;
import library.management.Fragments.BookTypeFragment;
import library.management.R;

public class BookTypeAdapter extends ArrayAdapter<BookType> {
    private final Context context;
    private final ArrayList<BookType> lists;
    BookTypeFragment fragment;
    TextView tvBookTypeId, tvBookType;
    ImageView imgDelBT;

    public BookTypeAdapter(@NonNull Context context, BookTypeFragment fragment, ArrayList<BookType> lists) {
        super(context, 0, lists);
        this.context = context;
        this.lists = lists;
        this.fragment = fragment;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.booktype_item, null);
        }
        final BookType item = lists.get(position);
        if (item != null) {

            tvBookTypeId = v.findViewById(R.id.tvBookTypeId);
            tvBookTypeId.setText(getContext().getResources().getString(R.string.ed_book_type_id) + item.bookTypeId);
            tvBookType = v.findViewById(R.id.tvBookType);
            tvBookType.setText(getContext().getResources().getString(R.string.ed_book_type_name) + item.bookType);

            imgDelBT = v.findViewById(R.id.imgDelBT);
        }
        imgDelBT.setOnClickListener(v1 -> {
            assert item != null;
            fragment.xoa(String.valueOf(item.bookTypeId));
        });
        return v;
    }
}
