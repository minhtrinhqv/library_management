package library.management.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import library.management.Entity.Book;
import library.management.Entity.BookBill;
import library.management.Entity.Member;
import library.management.Fragments.BookBillFragment;
import library.management.MyDatabase.BookDAO;
import library.management.MyDatabase.MemberDAO;
import library.management.R;

public class BookBillAdapter extends ArrayAdapter<BookBill> {
    private final Context context;
    private final ArrayList<BookBill> lists;
    BookBillFragment fragment;
    TextView tvBillId, tvMemberId, tvBookId, tvPrice, tvReturned, tvDate;
    ImageView imgDelBB;
    BookDAO bookDAO;
    MemberDAO memberDAO;

    public BookBillAdapter(@NonNull Context context, BookBillFragment fragment, ArrayList<BookBill> lists) {
        super(context, 0, lists);
        this.context = context;
        this.lists = lists;
        this.fragment = fragment;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.bookbill_item, null);
        }
        final BookBill item = lists.get(position);
        if (item != null) {

            tvBillId = v.findViewById(R.id.tvBillId);
            tvBillId.setText(getContext().getResources().getString(R.string.ed_bill_id) + item.bookId);
            bookDAO = new BookDAO(context);
            Book book = bookDAO.getID(String.valueOf(item.bookId));
            tvBookId = v.findViewById(R.id.tvBookId);
            tvBookId.setText(getContext().getResources().getString(R.string.ed_book_name)+ book.bookName);
            memberDAO = new MemberDAO(context);
            Member member = memberDAO.getID(String.valueOf(item.memberId));
            tvMemberId = v.findViewById(R.id.tvMemberId);
            tvMemberId.setText(getContext().getResources().getString(R.string.ed_member_name) + member.memberName);
            tvPrice = v.findViewById(R.id.tvPrice);
            tvPrice.setText(getContext().getResources().getString(R.string.ed_price) + item.price);
            tvDate = v.findViewById(R.id.tvDate);
            tvDate.setText(getContext().getResources().getString(R.string.ed_date) + item.date);
            tvReturned = v.findViewById(R.id.tvReturned);
            if (item.returned == 1) {
                tvReturned.setText(getContext().getResources().getString(R.string.ed_returned));
                tvReturned.setTextColor(Color.BLUE);
            } else {
                tvReturned.setTextColor(Color.RED);
                tvReturned.setText(getContext().getResources().getString(R.string.ed_not_returned));
            }
            imgDelBB = v.findViewById(R.id.imgDelBB);
        }
        imgDelBB.setOnClickListener(v1 -> {
            assert item != null;
            fragment.xoa(String.valueOf(item.billId));
        });
        return v;
    }
}

