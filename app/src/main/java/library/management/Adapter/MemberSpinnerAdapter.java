package library.management.Adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

import library.management.Entity.Member;
import library.management.R;

public class MemberSpinnerAdapter extends ArrayAdapter<Member> {
    private final Context context;
    private final ArrayList<Member> lists;
    TextView tvMemberIdSp, tvMemberNameSp;

    public MemberSpinnerAdapter(@NonNull Context context, ArrayList<Member> lists) {
        super(context, 0, lists);
        this.context = context;
        this.lists = lists;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.member_item_spinner, null);
        }
        final Member item = lists.get(position);
        if (item != null) {
            tvMemberIdSp = v.findViewById(R.id.tvMemberIdSp);
            tvMemberIdSp.setText(item.memberId + ".");
            tvMemberNameSp = v.findViewById(R.id.tvMemberNameSp);
            tvMemberNameSp.setText(item.memberName);
        }
        return v;
    }

    @SuppressLint({"InflateParams", "SetTextI18n"})
    @Override
    public View getDropDownView(int position, @Nullable View convertView, @NonNull ViewGroup parent) {
        View v = convertView;
        if (v == null) {
            LayoutInflater inflater = (LayoutInflater)
                    context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.member_item_spinner, null);
        }
        final Member item = lists.get(position);
        if (item != null) {
            tvMemberIdSp = v.findViewById(R.id.tvMemberIdSp);
            tvMemberIdSp.setText(item.memberId + ".");
            tvMemberNameSp = v.findViewById(R.id.tvMemberNameSp);
            tvMemberNameSp.setText(item.memberName);
        }
        return v;
    }
}
