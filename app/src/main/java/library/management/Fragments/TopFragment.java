package library.management.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.fragment.app.Fragment;

import java.util.ArrayList;

import library.management.Adapter.TopAdapter;
import library.management.Entity.Top;
import library.management.MyDatabase.StatisticsDAO;
import library.management.R;

public class TopFragment extends Fragment {
    ListView listView;
    ArrayList<Top> list;
    TopAdapter adapter;
    StatisticsDAO statisticsDAO;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_top, container, false);
        listView = v.findViewById(R.id.lvTop);
        statisticsDAO = new StatisticsDAO(getActivity());
        updateLv();
        return v;
    }

    void updateLv() {
        list = (ArrayList<Top>) statisticsDAO.getTop();
        adapter = new TopAdapter(requireActivity(), this, list);
        listView.setAdapter(adapter);
    }
}