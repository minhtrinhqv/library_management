package library.management.Fragments;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import library.management.Entity.Librarian;
import library.management.MyDatabase.LibrarianDAO;
import library.management.R;


public class AddUserFragment extends Fragment {
    LibrarianDAO dao;
    TextInputEditText edUserNew, edNameNew, edUPassNew, edReUPassNew;
    Button btnSaveUN, btnCancelUN;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_add_user, container, false);
        edUserNew = v.findViewById(R.id.edUserNew);
        edNameNew = v.findViewById(R.id.edNameNew);
        edUPassNew = v.findViewById(R.id.edUPassNew);
        edReUPassNew = v.findViewById(R.id.edReUPassNew);
        btnSaveUN = v.findViewById(R.id.btnSaveUN);
        btnCancelUN = v.findViewById(R.id.btnCancelUN);
        dao = new LibrarianDAO(getActivity());

        btnCancelUN.setOnClickListener(v1 -> {
            edUserNew.setText("");
            edNameNew.setText("");
            edUPassNew.setText("");
            edReUPassNew.setText("");
        });
        btnSaveUN.setOnClickListener(v12 -> {
            Librarian librarian = new Librarian();
            librarian.librarianId = Objects.requireNonNull(edUserNew.getText()).toString();
            librarian.libName = Objects.requireNonNull(edNameNew.getText()).toString();
            librarian.password = Objects.requireNonNull(edUPassNew.getText()).toString();
            if (validate() > 0) {
                if (dao.insert(librarian) > 0) {
                    Toast.makeText(getContext(), getResources().getString(R.string.msg_save), Toast.LENGTH_SHORT).show();
                    edUserNew.setText("");
                    edNameNew.setText("");
                    edUPassNew.setText("");
                    edReUPassNew.setText("");
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.msg_not_save), Toast.LENGTH_SHORT).show();
                }
            }
        });
        return v;
    }

    private int validate() {
        int check = 1;
        if (Objects.requireNonNull(edUserNew.getText()).length() == 0 || Objects.requireNonNull(edNameNew.getText()).length() == 0 || Objects.requireNonNull(edUPassNew.getText()).length() == 0 || Objects.requireNonNull(edReUPassNew.getText()).length() == 0) {
            Toast.makeText(getContext(), getResources().getString(R.string.msg_empty_register), Toast.LENGTH_SHORT).show();
            check = -1;
        } else {
            String pass = edUPassNew.getText().toString();
            String rePass = edReUPassNew.getText().toString();

            if (!pass.equals(rePass)) {
                Toast.makeText(getContext(), getResources().getString(R.string.msg_incorrect_password), Toast.LENGTH_SHORT).show();
            }
        }
        return check;
    }
}