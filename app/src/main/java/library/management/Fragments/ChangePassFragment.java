package library.management.Fragments;

import android.content.Context;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.textfield.TextInputEditText;

import java.util.Objects;

import library.management.Entity.Librarian;
import library.management.MyDatabase.LibrarianDAO;
import library.management.R;


public class ChangePassFragment extends Fragment {
    LibrarianDAO dao;
    TextInputEditText edPassOld, edPassNew, edRePassNew;
    Button btnSavePN, btnCancelPN;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_change_pass, container, false);
        edPassOld = v.findViewById(R.id.edPassOld);
        edPassNew = v.findViewById(R.id.edPassNew);
        edRePassNew = v.findViewById(R.id.edRePassNew);
        btnSavePN = v.findViewById(R.id.btnSavePN);
        btnCancelPN = v.findViewById(R.id.btnCancelPN);
        dao = new LibrarianDAO(getActivity());
        btnCancelPN.setOnClickListener(v1 -> {
            edPassOld.setText("");
            edPassNew.setText("");
            edRePassNew.setText("");
        });
        btnSavePN.setOnClickListener(v12 -> {
            SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("USER_FILE", Context.MODE_PRIVATE);
            String user = sharedPreferences.getString("USERNAME", "");
            if (validate() > 0) {
                Librarian librarian = dao.getID(user);
                librarian.password = Objects.requireNonNull(edPassNew.getText()).toString();
                if (dao.update(librarian) > 0) {
                    Toast.makeText(getContext(), getResources().getString(R.string.msg_change_password), Toast.LENGTH_SHORT).show();
                    edPassOld.setText("");
                    edPassNew.setText("");
                    edRePassNew.setText("");
                } else {
                    Toast.makeText(getContext(), getResources().getString(R.string.msg_not_change_password), Toast.LENGTH_SHORT).show();
                }
            }
        });
        return v;
    }

    private int validate() {
        int check = 1;
        if (Objects.requireNonNull(edPassOld.getText()).length() == 0 || Objects.requireNonNull(edPassNew.getText()).length() == 0 || Objects.requireNonNull(edRePassNew.getText()).length() == 0) {
            Toast.makeText(getContext(), getResources().getString(R.string.msg_empty_register), Toast.LENGTH_SHORT).show();
            check = -1;
        } else {
            SharedPreferences sharedPreferences = requireActivity().getSharedPreferences("USER_FILE", Context.MODE_PRIVATE);
            String passOld = sharedPreferences.getString("PASSWORD", "");
            String pass = edPassNew.getText().toString();
            String rePass = edRePassNew.getText().toString();
            if (!passOld.equals(edPassOld.getText().toString())) {
                Toast.makeText(getContext(), getResources().getString(R.string.msg_not_update), Toast.LENGTH_SHORT).show();
                check = -1;
            }
            if (!pass.equals(rePass)) {
                Toast.makeText(getContext(), getResources().getString(R.string.msg_incorrect_password), Toast.LENGTH_SHORT).show();

            }
        }
        return check;
    }
}