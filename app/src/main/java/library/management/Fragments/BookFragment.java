package library.management.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import library.management.Adapter.BookAdapter;
import library.management.Adapter.BookTypeSpinnerAdapter;
import library.management.Entity.Book;
import library.management.Entity.BookType;
import library.management.MyDatabase.BookDAO;
import library.management.MyDatabase.BookTypeDAO;
import library.management.R;

public class BookFragment extends Fragment {
    static BookDAO dao;
    ListView lv;
    ArrayList<Book> list;
    FloatingActionButton fab;
    Dialog dialog;
    EditText edBookId, edBookName, edPrice;
    Spinner spBookType;
    Button btnSave, btnCancel;
    BookAdapter adapter;
    Book item;
    BookTypeSpinnerAdapter spinnerAdapter;
    ArrayList<BookType> listBookTypes;
    BookTypeDAO bookTypeDAO;
    int bookTypeId, position;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_book, container, false);
        lv = v.findViewById(R.id.lvBook);
        fab = v.findViewById(R.id.fab);
        dao = new BookDAO(getActivity());
        updateLv();
        fab.setOnClickListener(v1 -> {
            if (dao.checkBookType() > 0) {
                Toast.makeText(getContext(), getResources().getString(R.string.msg_no_book_type), Toast.LENGTH_SHORT).show();
            } else {
                openDialog(getActivity(), 0);
            }
        });
        lv.setOnItemLongClickListener((parent, view, position, id) -> {
            item = list.get(position);
            openDialog(getActivity(), 1);
            return false;
        });
        return v;
    }

    protected void openDialog(final Context context, final int type) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.book_dialog);
        edBookId = dialog.findViewById(R.id.edBookId);
        edBookName = dialog.findViewById(R.id.edBookName);
        edPrice = dialog.findViewById(R.id.edPrice);
        edPrice.setInputType(InputType.TYPE_CLASS_NUMBER);
        spBookType = dialog.findViewById(R.id.spBookType);
        btnSave = dialog.findViewById(R.id.btnSave);
        btnCancel = dialog.findViewById(R.id.btnCancel);

        listBookTypes = new ArrayList<>();
        bookTypeDAO = new BookTypeDAO(context);
        listBookTypes = (ArrayList<BookType>) bookTypeDAO.getAll();
        spinnerAdapter = new BookTypeSpinnerAdapter(context, listBookTypes);
        spBookType.setAdapter(spinnerAdapter);

        spBookType.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bookTypeId = listBookTypes.get(position).bookTypeId;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        edBookId.setEnabled(false);
        if (type != 0) {
            edBookId.setText(String.valueOf(item.bookId));
            edBookName.setText(item.bookName);
            edPrice.setText(String.valueOf(item.price));
            for (int i = 0; i < listBookTypes.size(); i++)
                if (item.bookTypeId == (listBookTypes.get(i).bookTypeId)) {
                    position = i;
                }
            spBookType.setSelection(position);
        }
        btnCancel.setOnClickListener(v -> dialog.dismiss());
        btnSave.setOnClickListener(v -> {
            if (validate() > 0) {
                item = new Book();
                item.bookName = edBookName.getText().toString();
                item.price = Integer.parseInt(edPrice.getText().toString());
                item.bookTypeId = bookTypeId;

                if (type == 0) {
                    if (dao.insert(item) > 0) {
                        Toast.makeText(context, getResources().getString(R.string.msg_add), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.msg_not_add), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    item.bookId = Integer.parseInt(edBookId.getText().toString());
                    if (dao.update(item) > 0) {
                        Toast.makeText(context, getResources().getString(R.string.msg_update), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.msg_not_update), Toast.LENGTH_SHORT).show();
                    }
                }
                updateLv();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void xoa(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete");
        builder.setMessage(getResources().getString(R.string.msg_delete));
        builder.setCancelable(true);
        builder.setPositiveButton("Yes", (dialog, which) -> {
            dao.delete(id);
            updateLv();
            dialog.cancel();
        });
        builder.setNegativeButton("No", (dialog, which) -> dialog.cancel());
        builder.create();
        builder.show();
    }

    void updateLv() {
        list = (ArrayList<Book>) dao.getAll();
        adapter = new BookAdapter(requireActivity(), this, list);
        lv.setAdapter(adapter);
    }

    private int validate() {
        int check = 1;
        if (edBookName.getText().length() == 0 || edPrice.getText().length() == 0) {
            Toast.makeText(getContext(), getResources().getString(R.string.msg_empty_register), Toast.LENGTH_SHORT).show();
            check = -1;
        } else if (edPrice.getText().length() != 0) {
            int gia = Integer.parseInt(edPrice.getText().toString());
            if (gia < 0 || gia > 500000) {
                Toast.makeText(getContext(), getResources().getString(R.string.msg_correct_price), Toast.LENGTH_SHORT).show();
                check = -1;
            }
        }
        return check;
    }
}