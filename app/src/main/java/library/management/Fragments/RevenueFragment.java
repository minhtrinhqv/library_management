package library.management.Fragments;

import android.annotation.SuppressLint;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.fragment.app.Fragment;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;

import library.management.MyDatabase.StatisticsDAO;
import library.management.R;

public class RevenueFragment extends Fragment {
    Button btnFromDay, btnToDay, btnRevenue;
    EditText edFromDay, edToDay;
    TextView tvRevenue;
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
    int mYear, mMonth, mDay;

    @SuppressLint("SetTextI18n")
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View v = inflater.inflate(R.layout.fragment_revenue, container, false);
        edFromDay = v.findViewById(R.id.edFromDay);
        edToDay = v.findViewById(R.id.edToDay);
        tvRevenue = v.findViewById(R.id.tvRevenue);
        btnFromDay = v.findViewById(R.id.btnFromDay);
        btnToDay = v.findViewById(R.id.btnToDay);
        btnRevenue = v.findViewById(R.id.btnRevenue);

        DatePickerDialog.OnDateSetListener mDateFrom = (view, year, month, dayOfMonth) -> {
            mYear = year;
            mMonth = month;
            mDay = dayOfMonth;
            GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
            edFromDay.setText(sdf.format(c.getTime()));
        };
        DatePickerDialog.OnDateSetListener mDateTo = (view, year, month, dayOfMonth) -> {
            mYear = year;
            mMonth = month;
            mDay = dayOfMonth;
            GregorianCalendar c = new GregorianCalendar(mYear, mMonth, mDay);
            edToDay.setText(sdf.format(c.getTime()));
        };
        btnFromDay.setOnClickListener(v1 -> {
            Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog d = new DatePickerDialog(getActivity(),
                    0, mDateFrom, mYear, mMonth, mDay);
            d.show();
        });
        btnToDay.setOnClickListener(v12 -> {
            Calendar c = Calendar.getInstance();
            mYear = c.get(Calendar.YEAR);
            mMonth = c.get(Calendar.MONTH);
            mDay = c.get(Calendar.DAY_OF_MONTH);
            DatePickerDialog d = new DatePickerDialog(getActivity(),
                    0, mDateTo, mYear, mMonth, mDay);
            d.show();
        });
        btnRevenue.setOnClickListener(v13 -> {
            String fromDay = edFromDay.getText().toString();
            String toDay = edToDay.getText().toString();
            StatisticsDAO statisticsDAO = new StatisticsDAO(getActivity());
            tvRevenue.setText(requireContext().getResources().getString(R.string.ed_revenue) + statisticsDAO.getStatistics(fromDay, toDay) + "VNĐ");
        });
        return v;
    }

}