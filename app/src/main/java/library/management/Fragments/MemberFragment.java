package library.management.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import library.management.Adapter.MemberAdapter;
import library.management.Entity.Member;
import library.management.MyDatabase.MemberDAO;
import library.management.R;


public class MemberFragment extends Fragment {
    ListView lv;
    ArrayList<Member> list;
    FloatingActionButton fab;
    Dialog dialog;
    EditText edMemberId, edMemberName, edYearOfBirth;
    Button btnSave, btnCancel;
    MemberDAO dao;
    MemberAdapter adapter;
    Member item;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_member, container, false);
        lv = v.findViewById(R.id.lvMember);
        fab = v.findViewById(R.id.fab);
        dao = new MemberDAO(getActivity());
        updateLv();
        fab.setOnClickListener(v1 -> openDialog(getActivity(), 0));
        lv.setOnItemLongClickListener((parent, view, position, id) -> {
            item = list.get(position);
            openDialog(getActivity(), 1);
            return false;
        });
        return v;
    }

    protected void openDialog(final Context context, final int type) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.member_dialog);
        edMemberId = dialog.findViewById(R.id.edMemberId);
        edMemberName = dialog.findViewById(R.id.edMemberName);
        edYearOfBirth = dialog.findViewById(R.id.edYearOfBirth);
        edYearOfBirth.setInputType(InputType.TYPE_CLASS_NUMBER);
        btnSave = dialog.findViewById(R.id.btnSave);
        btnCancel = dialog.findViewById(R.id.btnCancel);
        edMemberId.setEnabled(false);
        if (type != 0) {
            edMemberId.setText(String.valueOf(item.memberId));
            edMemberName.setText(item.memberName);
            edYearOfBirth.setText(item.yearOfBirth);
        }
        btnCancel.setOnClickListener(v -> dialog.dismiss());
        btnSave.setOnClickListener(v -> {
            item = new Member();
            item.memberName = edMemberName.getText().toString();
            item.yearOfBirth = edYearOfBirth.getText().toString();
            if (validate() > 0) {
                if (type == 0) {
                    if (dao.insert(item) > 0) {
                        Toast.makeText(context, getResources().getString(R.string.msg_add), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.msg_not_add), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    item.memberId = Integer.parseInt(edMemberId.getText().toString());
                    if (dao.update(item) > 0) {
                        Toast.makeText(context, getResources().getString(R.string.msg_update), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.msg_not_update), Toast.LENGTH_SHORT).show();
                    }
                }
                updateLv();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void xoa(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete");
        builder.setMessage(getResources().getString(R.string.msg_delete));
        builder.setCancelable(true);
        builder.setPositiveButton("Yes", (dialog, which) -> {
            dao.delete(id);
            updateLv();
            dialog.cancel();
        });
        builder.setNegativeButton("No", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    void updateLv() {
        list = (ArrayList<Member>) dao.getAll();
        adapter = new MemberAdapter(requireActivity(), this, list);
        lv.setAdapter(adapter);
    }

    private int validate() {
        int check = 1;
        if (edMemberName.getText().length() == 0 || edYearOfBirth.getText().length() == 0) {
            Toast.makeText(getContext(), getResources().getString(R.string.msg_empty_register), Toast.LENGTH_SHORT).show();
            check = -1;
        } else if (edYearOfBirth.getText().length() != 0) {
            int birthday = Integer.parseInt(edYearOfBirth.getText().toString());
            if (birthday < 1900 || birthday > 2021) {
                Toast.makeText(getContext(), getResources().getString(R.string.msg_correct_birthday), Toast.LENGTH_SHORT).show();
                check = -1;
            }
        }
        return check;
    }
}