package library.management.Fragments;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;

import library.management.Adapter.BookBillAdapter;
import library.management.Adapter.BookSpinnerAdapter;
import library.management.Adapter.MemberSpinnerAdapter;
import library.management.Entity.Book;
import library.management.Entity.BookBill;
import library.management.Entity.Member;
import library.management.MyDatabase.BookBillDAO;
import library.management.MyDatabase.BookDAO;
import library.management.MyDatabase.MemberDAO;
import library.management.R;

public class BookBillFragment extends Fragment {

    static BookBillDAO dao;
    ListView lv;
    ArrayList<BookBill> list;
    FloatingActionButton fab;
    Dialog dialog;
    EditText edBillId;
    Spinner spMemberName, spBookName;
    TextView tvPrice, tvDate;
    CheckBox chkReturned;
    Button btnSave, btnCancel;
    BookBillAdapter adapter;
    BookBill item;
    MemberSpinnerAdapter memberSpinnerAdapter;
    ArrayList<Member> listMember;
    MemberDAO memberDAO;
    int memberId;
    BookSpinnerAdapter bookSpinnerAdapter;
    ArrayList<Book> listBook;
    BookDAO bookDAO;
    int bookId, price;
    int positionTV, positionBook;
    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_book_bill, container, false);
        lv = v.findViewById(R.id.lvBookBill);
        fab = v.findViewById(R.id.fab);
        dao = new BookBillDAO(getActivity());
        updateLv();
        fab.setOnClickListener(v1 -> {
            if (dao.checkTV() > 0) {
                Toast.makeText(getContext(), getResources().getString(R.string.msg_no_member), Toast.LENGTH_SHORT).show();
            } else if (dao.checkBook() > 0) {
                Toast.makeText(getContext(), getResources().getString(R.string.msg_no_member), Toast.LENGTH_SHORT).show();
            } else {
                openDialog(getActivity(), 0);
            }
        });
        lv.setOnItemLongClickListener((parent, view, position, id) -> {
            item = list.get(position);
            openDialog(getActivity(), 1);//update
            return false;
        });
        return v;
    }

    @SuppressLint("SetTextI18n")
    protected void openDialog(final Context context, final int type) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.bookbill_dialog);
        edBillId = dialog.findViewById(R.id.edBillId);
        spMemberName = dialog.findViewById(R.id.spMemberName);
        spBookName = dialog.findViewById(R.id.spBookName);
        tvDate = dialog.findViewById(R.id.tvDate);
        tvPrice = dialog.findViewById(R.id.tvPrice);
        chkReturned = dialog.findViewById(R.id.chkReturned);
        btnSave = dialog.findViewById(R.id.btnSave);
        btnCancel = dialog.findViewById(R.id.btnCancel);

        memberDAO = new MemberDAO(context);
        listMember = new ArrayList<>();
        listMember = (ArrayList<Member>) memberDAO.getAll();
        memberSpinnerAdapter = new MemberSpinnerAdapter(context, listMember);
        spMemberName.setAdapter(memberSpinnerAdapter);
        spMemberName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                memberId = listMember.get(position).memberId;
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        bookDAO = new BookDAO(context);
        listBook = new ArrayList<>();
        listBook = (ArrayList<Book>) bookDAO.getAll();
        bookSpinnerAdapter = new BookSpinnerAdapter(context, listBook);
        spBookName.setAdapter(bookSpinnerAdapter);
        spBookName.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @SuppressLint("SetTextI18n")
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                bookId = listBook.get(position).bookId;
                price = listBook.get(position).price;
                tvPrice.setText(requireContext().getResources().getString(R.string.ed_price) + price);
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        tvDate.setText(requireContext().getResources().getString(R.string.ed_date) + now());

        edBillId.setEnabled(false);
        if (type != 0) {
            edBillId.setText(String.valueOf(item.billId));
            for (int i = 0; i < listMember.size(); i++)
                if (item.memberId == (listMember.get(i).memberId)) {
                    positionTV = i;
                }
            spMemberName.setSelection(positionTV);
            for (int i = 0; i < listBook.size(); i++)
                if (item.bookId == (listBook.get(i).bookId)) {
                    positionBook = i;
                }
            spBookName.setSelection(positionBook);
            tvDate.setText(requireContext().getResources().getString(R.string.ed_date) + item.date);
            tvPrice.setText(requireContext().getResources().getString(R.string.ed_price) + item.price);
            chkReturned.setChecked(item.returned == 1);
        }
        btnCancel.setOnClickListener(v -> dialog.dismiss());
        btnSave.setOnClickListener(v -> {
            item = new BookBill();
            item.bookId = bookId;
            item.memberId = memberId;
            item.date = java.sql.Date.valueOf(now());
            item.price = price;
            if (chkReturned.isChecked()) {
                item.returned = 1;
            } else {
                item.returned = 0;
            }
            if (validate() > 0) {
                if (type == 0) {
                    if (dao.insert(item) > 0) {
                        Toast.makeText(context, getResources().getString(R.string.msg_add), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.msg_not_add), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    item.billId = Integer.parseInt(edBillId.getText().toString());
                    if (dao.update(item) > 0) {
                        Toast.makeText(context, getResources().getString(R.string.msg_update), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.msg_not_update), Toast.LENGTH_SHORT).show();
                    }
                }
                updateLv();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private String now() {
        return sdf.format(Calendar.getInstance().getTime());
    }

    public void xoa(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete");
        builder.setMessage(getResources().getString(R.string.msg_delete));
        builder.setCancelable(true);
        builder.setPositiveButton("Yes", (dialog, which) -> {
            dao.delete(id);
            updateLv();
            dialog.cancel();
        });
        builder.setNegativeButton("No", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    void updateLv() {
        list = (ArrayList<BookBill>) dao.getAll();
        adapter = new BookBillAdapter(requireActivity(), this, list);
        lv.setAdapter(adapter);
    }

    private int validate() {
        return 1;
    }
}