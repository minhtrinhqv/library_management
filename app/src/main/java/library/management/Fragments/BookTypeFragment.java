package library.management.Fragments;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import java.util.ArrayList;

import library.management.Adapter.BookTypeAdapter;
import library.management.Entity.BookType;
import library.management.MyDatabase.BookTypeDAO;
import library.management.R;


public class BookTypeFragment extends Fragment {
    static BookTypeDAO dao;
    ListView lv;
    ArrayList<BookType> list;
    FloatingActionButton fab;
    Dialog dialog;
    EditText edBookTypeId, edBookType;
    Button btnSave, btnCancel;
    BookTypeAdapter adapter;
    BookType item;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View v = inflater.inflate(R.layout.fragment_book_type, container, false);
        lv = v.findViewById(R.id.lvBookType);
        fab = v.findViewById(R.id.fab);
        dao = new BookTypeDAO(getActivity());
        updateLv();
        fab.setOnClickListener(v1 -> openDialog(getActivity(), 0));
        lv.setOnItemLongClickListener((parent, view, position, id) -> {
            item = list.get(position);
            openDialog(getActivity(), 1);
            return false;
        });
        return v;
    }

    protected void openDialog(final Context context, final int type) {
        dialog = new Dialog(context);
        dialog.setContentView(R.layout.booktype_dialog);
        edBookTypeId = dialog.findViewById(R.id.edBookTypeId);
        edBookType = dialog.findViewById(R.id.edBookType);
        btnSave = dialog.findViewById(R.id.btnSave);
        btnCancel = dialog.findViewById(R.id.btnCancel);
        edBookTypeId.setEnabled(false);
        if (type != 0) {
            edBookTypeId.setText(String.valueOf(item.bookTypeId));
            edBookType.setText(item.bookType);
        }
        btnCancel.setOnClickListener(v -> dialog.dismiss());
        btnSave.setOnClickListener(v -> {
            item = new BookType();
            item.bookType = edBookType.getText().toString();
            if (validate() > 0) {
                if (type == 0) {
                    if (dao.insert(item) > 0) {
                        Toast.makeText(context, getResources().getString(R.string.msg_add), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.msg_not_add), Toast.LENGTH_SHORT).show();
                    }
                } else {
                    item.bookTypeId = Integer.parseInt(edBookTypeId.getText().toString());
                    if (dao.update(item) > 0) {
                        Toast.makeText(context, getResources().getString(R.string.msg_update), Toast.LENGTH_SHORT).show();
                    } else {
                        Toast.makeText(context, getResources().getString(R.string.msg_not_update), Toast.LENGTH_SHORT).show();
                    }
                }
                updateLv();
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void xoa(final String id) {
        AlertDialog.Builder builder = new AlertDialog.Builder(getContext());
        builder.setTitle("Delete");
        builder.setMessage(getResources().getString(R.string.msg_delete));
        builder.setCancelable(true);
        builder.setPositiveButton("Yes", (dialog, which) -> {
            dao.delete(id);
            updateLv();
            dialog.cancel();
        });
        builder.setNegativeButton("No", (dialog, which) -> dialog.cancel());
        builder.show();
    }

    void updateLv() {
        list = (ArrayList<BookType>) dao.getAll();
        adapter = new BookTypeAdapter(requireActivity(), this, list);
        lv.setAdapter(adapter);
    }

    private int validate() {
        int check = 1;
        if (edBookType.getText().length() == 0) {
            Toast.makeText(getContext(), getResources().getString(R.string.msg_empty_register), Toast.LENGTH_SHORT).show();
            check = -1;
        }
        return check;
    }
}