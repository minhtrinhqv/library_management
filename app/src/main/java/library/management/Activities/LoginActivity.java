package library.management.Activities;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import library.management.Entity.Librarian;
import library.management.MyDatabase.LibrarianDAO;
import library.management.R;

public class LoginActivity extends AppCompatActivity {
    LibrarianDAO dao;
    EditText edUser, edPass;
    TextView tvNew;
    Button btnLogin;
    CheckBox checkBox;
    String strUser, strPass;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        edUser = findViewById(R.id.edUserName);
        edPass = findViewById(R.id.edPassword);
        btnLogin = findViewById(R.id.btnLogin);
        checkBox = findViewById(R.id.checkBox);
        tvNew = findViewById(R.id.tvNew);
        dao = new LibrarianDAO(this);
        //doc user pass
        SharedPreferences pref = getSharedPreferences("USER_FILE", MODE_PRIVATE);
        edUser.setText(pref.getString("USERNAME", ""));
        edPass.setText(pref.getString("PASSWORD", ""));
        checkBox.setChecked(pref.getBoolean("REMEMBER", false));

        tvNew.setOnClickListener(v -> {
            boolean checkThuThu = dao.checkLibrarian();
            if (checkThuThu) {
                Librarian thuThu = new Librarian("admin", "admin", "admin");
                dao.insert(thuThu);
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_create_admin), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_admin_exists), Toast.LENGTH_SHORT).show();
            }
        });
        btnLogin.setOnClickListener(v -> checkLogin());
    }

    public void checkLogin() {
        strUser = edUser.getText().toString();
        strPass = edPass.getText().toString();
        if (strUser.isEmpty() || strPass.isEmpty()) {
            Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_empty_login), Toast.LENGTH_SHORT).show();
        } else {
            if (!dao.checkAcc(strUser, strPass)) {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_incorrect_login), Toast.LENGTH_SHORT).show();
            } else {
                Toast.makeText(getApplicationContext(), getResources().getString(R.string.msg_login), Toast.LENGTH_SHORT).show();
                rememberUser(strUser, strPass, checkBox.isChecked());
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                intent.putExtra("user", strUser);
                startActivity(intent);
                finish();
            }
        }
    }

    private void rememberUser(String u, String p, boolean status) {
        SharedPreferences pref = getSharedPreferences("USER_FILE", MODE_PRIVATE);
        SharedPreferences.Editor edit = pref.edit();
        if (!status) {
            edit.clear();
        } else {
            edit.putString("USERNAME", u);
            edit.putString("PASSWORD", p);
            edit.putBoolean("REMEMBER", true);
        }
        edit.apply();
    }
}