package library.management.Activities;

import android.annotation.SuppressLint;
import android.content.Intent;
import android.os.Bundle;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.fragment.app.FragmentManager;

import com.google.android.material.navigation.NavigationView;

import library.management.Entity.Librarian;
import library.management.Fragments.AddUserFragment;
import library.management.Fragments.BookBillFragment;
import library.management.Fragments.BookFragment;
import library.management.Fragments.BookTypeFragment;
import library.management.Fragments.ChangePassFragment;
import library.management.Fragments.MemberFragment;
import library.management.Fragments.RevenueFragment;
import library.management.Fragments.TopFragment;
import library.management.MyDatabase.LibrarianDAO;
import library.management.R;

public class MainActivity extends AppCompatActivity {
    DrawerLayout drawerLayout;
    Toolbar toolbar;
    View mHeaderView;
    TextView edUser;
    LibrarianDAO librarianDAO;
    FragmentManager manager;
    Librarian librarian;

    @SuppressLint({"SetTextI18n", "NonConstantResourceId"})
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        drawerLayout = findViewById(R.id.drawer_layout);
        toolbar = findViewById(R.id.toolbar);

        //set toolbar
        setSupportActionBar(toolbar);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar,
                R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(toggle);
        toggle.syncState();

        setTitle(getResources().getString(R.string.txt_bill));
        manager = getSupportFragmentManager();
        BookBillFragment bookBillFragment = new BookBillFragment();
        manager.beginTransaction()
                .replace(R.id.flContent, bookBillFragment)
                .commit();
        NavigationView nv = findViewById(R.id.nav_view);

        mHeaderView = nv.getHeaderView(0);
        edUser = mHeaderView.findViewById(R.id.tvUser);
        Intent i = getIntent();
        String user = i.getStringExtra("user");
        librarianDAO = new LibrarianDAO(this);
        librarian = librarianDAO.getID(user);
        String userName = librarian.libName;
        edUser.setText("Welcome " + userName + "!");


        if (user.equalsIgnoreCase("admin")) {
            nv.getMenu().findItem(R.id.sub_adduser).setVisible(true);
        }

        nv.setNavigationItemSelectedListener(item -> {
            manager = getSupportFragmentManager();
            switch (item.getItemId()) {
                case R.id.nav_bill:
                    setTitle(getResources().getString(R.string.txt_bill));
                    BookBillFragment bookBillFragment1 = new BookBillFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent, bookBillFragment1)
                            .commit();
                    break;
                case R.id.nav_booktype:
                    setTitle(getResources().getString(R.string.txt_book_type));
                    BookTypeFragment bookTypeFragment = new BookTypeFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent, bookTypeFragment)
                            .commit();
                    break;
                case R.id.nav_book:
                    setTitle(getResources().getString(R.string.txt_book));
                    BookFragment bookFragment = new BookFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent, bookFragment)
                            .commit();
                    break;
                case R.id.nav_member:
                    setTitle(getResources().getString(R.string.txt_member));
                    MemberFragment memberFragment = new MemberFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent, memberFragment)
                            .commit();
                    break;
                case R.id.sub_top:
                    setTitle(getResources().getString(R.string.txt_top));
                    TopFragment topFragment = new TopFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent, topFragment)
                            .commit();
                    break;
                case R.id.sub_revenue:
                    setTitle(getResources().getString(R.string.txt_statistics_revenue));
                    RevenueFragment revenueFragment = new RevenueFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent, revenueFragment)
                            .commit();
                    break;
                case R.id.sub_adduser:
                    setTitle(getResources().getString(R.string.menu_add_user));
                    AddUserFragment addUserFragment = new AddUserFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent, addUserFragment)
                            .commit();
                    break;
                case R.id.sub_pass:
                    setTitle(getResources().getString(R.string.txt_change_password));
                    ChangePassFragment changePassFragment = new ChangePassFragment();
                    manager.beginTransaction()
                            .replace(R.id.flContent, changePassFragment)
                            .commit();
                    break;
                case R.id.sub_logout:
                    startActivity(new Intent(getApplicationContext(), LoginActivity.class));
                    finish();
                    break;
            }
            drawerLayout.closeDrawers();
            return false;
        });


    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            drawerLayout.openDrawer(GravityCompat.START);
        }
        return super.onOptionsItemSelected(item);
    }
}