package library.management.MyDatabase;

import android.annotation.SuppressLint;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import library.management.Entity.Book;
import library.management.Entity.Top;

public class StatisticsDAO {
    private final SQLiteDatabase db;
    private final Context context;

    public StatisticsDAO(Context context) {
        this.context = context;
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //top10
    public List<Top> getTop() {
        String sqlTop = "SELECT bookId, count(bookId) as count FROM BookBill " +
                "GROUP BY bookId ORDER BY count DESC LIMIT 10";
        List<Top> topList = new ArrayList<>();
        BookDAO bookDAO = new BookDAO(context);
        @SuppressLint("Recycle") Cursor c = db.rawQuery(sqlTop, null);
        while (c.moveToNext()) {
            Top top = new Top();
            Book book = bookDAO.getID(c.getString(c.getColumnIndex("bookId")));
            top.bookName = book.bookName;
            top.count = Integer.parseInt(c.getString(c.getColumnIndex("count")));
            topList.add(top);

        }
        return topList;


    }


    public int getStatistics(String fromDay, String toDay) {
        String sqlStatistics = "SELECT SUM(price) as statistics FROM BookBill " +
                "WHERE date BETWEEN ? AND ?";
        List<Integer> list = new ArrayList<>();
        @SuppressLint("Recycle") Cursor c = db.rawQuery(sqlStatistics, new String[]{fromDay, toDay});
        while (c.moveToNext()) {
            try {
                list.add(Integer.parseInt(c.getString(c.getColumnIndex("statistics"))));
            } catch (Exception e) {
                list.add(0);
            }
        }
        return list.get(0);
    }
}
