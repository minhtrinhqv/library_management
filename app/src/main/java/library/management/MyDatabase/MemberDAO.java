package library.management.MyDatabase;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import library.management.Entity.Member;

public class MemberDAO {
    private final SQLiteDatabase db;
    private final String name = "memberName";
    private final String birthday = "yearOfBirth";

    public MemberDAO(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //insert
    public long insert(Member obj) {
        ContentValues values = new ContentValues();
        values.put(name, obj.memberName);
        values.put(birthday, obj.yearOfBirth);

        return db.insert("Member", null, values);
    }

    //update
    public int update(Member obj) {
        ContentValues values = new ContentValues();
        values.put(name, obj.memberName);
        values.put(birthday, obj.yearOfBirth);

        return db.update("Member", values, "memberId=?", new String[]{String.valueOf(obj.memberId)});
    }

    //delete
    public void delete(String id) {
        db.delete("Member", "memberId=?", new String[]{id});
    }

    // get tat ca data
    public List<Member> getAll() {
        String sql = "SELECT * FROM Member";
        return getData(sql);
    }


    //getData theo id
    public Member getID(String id) {
        String sql = "SELECT * FROM Member WHERE memberId=?";
        List<Member> memberList = getData(sql, id);
        return memberList.get(0);
    }

    private List<Member> getData(String sql, String... selectionArgs) {
        List<Member> list = new ArrayList<>();
        @SuppressLint("Recycle") Cursor c = db.rawQuery(sql, selectionArgs);
        while (c.moveToNext()) {
            Member obj = new Member();
            obj.memberId = Integer.parseInt(c.getString(c.getColumnIndex("memberId")));
            obj.memberName = c.getString(c.getColumnIndex("memberName"));
            obj.yearOfBirth = c.getString(c.getColumnIndex("yearOfBirth"));
            list.add(obj);
        }
        return list;
    }


}

