package library.management.MyDatabase;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import library.management.Entity.BookBill;

public class BookBillDAO {
    final String libId = "librarianId";
    final String memberId = "memberId";
    final String bookId = "bookId";
    final String price = "price";
    final String returned = "returned";
    final String date = "date";
    private final SQLiteDatabase db;

    public BookBillDAO(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //insert
    public long insert(BookBill obj) {
        ContentValues values = new ContentValues();
        values.put(libId, obj.librarianId);
        values.put(memberId, obj.memberId);
        values.put(bookId, obj.bookId);
        values.put(price, obj.price);
        values.put(returned, obj.returned);
        values.put(date, String.valueOf(obj.date));
        return db.insert("BookBill", null, values);
    }

    //update
    public int update(BookBill obj) {
        ContentValues values = new ContentValues();
        values.put(libId, obj.librarianId);
        values.put(memberId, obj.memberId);
        values.put(bookId, obj.bookId);
        values.put(price, obj.price);
        values.put(returned, obj.returned);
        values.put(date, String.valueOf(obj.date));
        return db.update("BookBill", values, "billId=?", new String[]{String.valueOf(obj.billId)});
    }

    //delete
    public void delete(String id) {
        db.delete("BookBill", "billId=?", new String[]{id});
    }

    // get tat ca data
    public List<BookBill> getAll() {
        String sql = "SELECT * FROM BookBill";
        return getData(sql);
    }


    private List<BookBill> getData(String sql, String... selectionArgs) {
        List<BookBill> bookBillList = new ArrayList<>();
        @SuppressLint("Recycle") Cursor c = db.rawQuery(sql, selectionArgs);
        while (c.moveToNext()) {
            BookBill obj = new BookBill();
            obj.billId = Integer.parseInt(c.getString(c.getColumnIndex("billId")));
            obj.librarianId = c.getString(c.getColumnIndex("librarianId"));
            obj.memberId = Integer.parseInt(c.getString(c.getColumnIndex("memberId")));
            obj.bookId = Integer.parseInt(c.getString(c.getColumnIndex("bookId")));
            obj.price = Integer.parseInt(c.getString(c.getColumnIndex("price")));
            obj.returned = Integer.parseInt(c.getString(c.getColumnIndex("returned")));
            obj.date = java.sql.Date.valueOf(c.getString(c.getColumnIndex("date")));
            bookBillList.add(obj);
        }
        return bookBillList;
    }

    public int checkBook() {
        int check = 1;
        String getBook = "SELECT * FROM Book";
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(getBook, null);
        if (cursor.getCount() != 0) {
            check = -1;
        }
        return check;
    }

    public int checkTV() {
        int check = 1;
        String getTV = "SELECT * FROM Member";
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(getTV, null);
        if (cursor.getCount() != 0) {
            check = -1;
        }
        return check;
    }


}
