package library.management.MyDatabase;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DbHelper extends SQLiteOpenHelper {
    final String createTableLibrarian =
            "create table Librarian (librarianId TEXT PRIMARY KEY, libName TEXT NOT NULL, password TEXT NOT NULL )";
    final String createTableMember =
            "create table Member ( memberId INTEGER PRIMARY KEY , memberName TEXT NOT NULL, yearOfBirth TEXT NOT NULL )";
    final String createTableBookType =
            "create table BookType (bookTypeId INTEGER PRIMARY KEY AUTOINCREMENT, bookType TEXT NOT NULL )";
    final String createTableBook =
            "create table Book (bookId INTEGER PRIMARY KEY AUTOINCREMENT,  bookName TEXT NOT NULL,price INTEGER NOT NULL," +
                    " bookTypeId INTEGER REFERENCES bookType(bookTypeId))";
    final String createTableBookBill =
            "create table BookBill (billId INTEGER PRIMARY KEY AUTOINCREMENT,price INTEGER NOT NULL,date DATE NOT NULL," +
                    "returned INTEGER NOT NULL,librarianId TEXT REFERENCES Librarian(librarianId),memberId INTEGER REFERENCES Member(memberId),bookId INTEGER REFERENCES Book(bookId))";
    final String dropTableLibrarian = "drop table if exists Librarian";
    final String dropTableMember = "drop table if exists Member";
    final String dropTableBookType = "drop table if exists BookType";
    final String dropTableBook = "drop table if exists Book";
    final String dropTableBookBill = "drop table if exists BookBill";

    public DbHelper(Context context) {
        super(context, "library", null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL(createTableLibrarian);
        db.execSQL(createTableMember);
        db.execSQL(createTableBookType);
        db.execSQL(createTableBook);
        db.execSQL(createTableBookBill);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        db.execSQL(dropTableLibrarian);
        db.execSQL(dropTableMember);
        db.execSQL(dropTableBookType);
        db.execSQL(dropTableBook);
        db.execSQL(dropTableBookBill);
    }
}