package library.management.MyDatabase;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import library.management.Entity.BookType;

public class BookTypeDAO {
    final String bookType = "bookType";
    private final SQLiteDatabase db;

    public BookTypeDAO(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //insert
    public long insert(BookType obj) {
        ContentValues values = new ContentValues();
        values.put(bookType, obj.bookType);
        return db.insert("BookType", null, values);
    }

    //update
    public int update(BookType obj) {
        ContentValues values = new ContentValues();
        values.put(bookType, obj.bookType);
        return db.update("BookType", values, "bookTypeId=?", new String[]{String.valueOf(obj.bookTypeId)});
    }

    //delete
    public void delete(String id) {
        db.delete("BookType", "bookTypeId=?", new String[]{id});
    }

    // get tat ca data
    public List<BookType> getAll() {
        String sql = "SELECT * FROM BookType";
        return getData(sql);
    }

    //getData theo id
    public BookType getID(String id) {
        String sql = "SELECT * FROM BookType WHERE bookTypeId=?";
        List<BookType> list = getData(sql, id);
        return list.get(0);
    }

    private List<BookType> getData(String sql, String... selectionArgs) {
        List<BookType> list = new ArrayList<>();
        @SuppressLint("Recycle") Cursor c = db.rawQuery(sql, selectionArgs);
        while (c.moveToNext()) {
            BookType obj = new BookType();
            obj.bookTypeId = Integer.parseInt(c.getString(c.getColumnIndex("bookTypeId")));
            obj.bookType = c.getString(c.getColumnIndex("bookType"));
            list.add(obj);
        }
        return list;
    }
}
