package library.management.MyDatabase;

import android.annotation.SuppressLint;
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

import library.management.Entity.Librarian;

public class LibrarianDAO {
    final String libId = "librarianId";
    final String libName = "libName";
    final String password = "password";
    private final SQLiteDatabase db;

    public LibrarianDAO(Context context) {
        DbHelper dbHelper = new DbHelper(context);
        db = dbHelper.getWritableDatabase();
    }

    //insert
    public long insert(Librarian obj) {
        ContentValues values = new ContentValues();
        values.put(libId, obj.librarianId);
        values.put(libName, obj.libName);
        values.put(password, obj.password);
        return db.insert("Librarian", null, values);
    }

    //update
    public int update(Librarian obj) {
        ContentValues values = new ContentValues();
        values.put(libId, obj.librarianId);
        values.put(libName, obj.libName);
        values.put(password, obj.password);
        return db.update("Librarian", values, "librarianId=?", new String[]{String.valueOf(obj.librarianId)});
    }

    //getData theo id
    public Librarian getID(String id) {
        String sql = "SELECT * FROM Librarian WHERE librarianId=?";
        List<Librarian> list = getData(sql, id);
        return list.get(0);
    }

    private List<Librarian> getData(String sql, String... selectionArgs) {
        List<Librarian> list = new ArrayList<>();
        @SuppressLint("Recycle") Cursor c = db.rawQuery(sql, selectionArgs);
        while (c.moveToNext()) {
            Librarian obj = new Librarian();
            obj.librarianId = c.getString(c.getColumnIndex("librarianId"));
            obj.libName = c.getString(c.getColumnIndex("libName"));
            obj.password = c.getString(c.getColumnIndex("password"));
            list.add(obj);
        }
        return list;
    }

    public boolean checkLibrarian() {
        String getLibrarian = "SELECT * FROM Librarian";
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(getLibrarian, null);
        return cursor.getCount() == 0;
    }

    public boolean checkAcc(String strUser, String strPass) {
        String getAccount = "SELECT * FROM Librarian WHERE librarianId = '" + strUser + "' " +
                "AND password = '" + strPass + "'";
        @SuppressLint("Recycle") Cursor cursor = db.rawQuery(getAccount, null);
        return cursor.getCount() != 0;
    }
}

